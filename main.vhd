----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:54:09 11/04/2013 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port ( clk : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				led_0 : out  STD_LOGIC;
				led_1 : out  STD_LOGIC;
				led_2 : out  STD_LOGIC;
				led_3 : out  STD_LOGIC;
				led_4 : out  STD_LOGIC;
				led_5 : out  STD_LOGIC;
				led_6 : out  STD_LOGIC;
				led_7 : out  STD_LOGIC);
end main;

architecture Behavioral of main is

	signal cnt_led : std_logic_vector(31 downto 0);
	constant c_end : std_logic_vector(31 downto 0) := x"006940AA"; -- 03f9400a --7940aa
	signal led_i : std_logic;
	signal contador : std_logic_vector(7 downto 0);
begin

	
	p_cnt : process(clk, reset)
	begin
		if(reset = '0') then
			cnt_led <= (others => '0');
		elsif(clk'event and clk = '1') then
			if(cnt_led < c_end) then
				cnt_led <= cnt_led + 1;
			else
				cnt_led <= (others => '0');
			end if;
		end if;
	end process;
	
	p_led : process(clk, reset)
	begin
		if(reset = '0') then
			contador <= (others => '0');
		elsif (clk'event and clk = '1') then
			if cnt_led = c_end then
				contador <= contador + 1;
			end if;
		end if;
	end process;

	led_7 <= contador(7);
	led_6 <= contador(6);
	led_5 <= contador(5);
	led_4 <= contador(4);
	led_3 <= contador(3);
	led_2 <= contador(2);
	led_1 <= contador(1);
	led_0 <= contador(0);

end Behavioral;

